app.controller("studentController", function($scope, StudentService) {
	$scope.dvStudent = false;
	getStudentList();
	$scope.students = [];
	
	$scope.studentMain = StudentService.getStudentMain();

	// Get All Records
	function getStudentList() {
		StudentService.getAllStudents().success(function(stu) {
			$scope.students = stu;
		}).error(function() {
			alert('Error in getting records');
		});
	}

	// To display Add div
	$scope.AddNewStudent = function() {
		$scope.Action = "Add";
		$scope.dvStudent = true;
	}
	// Add new student record
	$scope.addStudnet = function(student) {
		StudentService.addNewStudent(student).success(function(msg) {
			$scope.students.push(student);
		}, function() {
			alert('Error in adding record');
		});
	}
	// Delete record.
	$scope.deleteStudent = function(stu, index) {
		var retval = StudentService.deleteStudent(stu.Id).success(
				function(msg) {
					$scope.students.splice(index, 1);
					// alert('Student has been deleted successfully.');
				}).error(function() {
			alert('Oops! something went wrong.');
		});
	}
	// update Student Records
	$scope.updateStudent = function(tbl_Student) {
		$scope.studentMain.id = tbl_Student.id;
		$scope.studentMain.name = tbl_Student.name;
		$scope.studentMain.email = tbl_Student.email;
		
	}
	
	// get Student
	$scope.getStudent = function(Id) {
		var RetValData = StudentService.getStudent(Id).success(
				function(msg) {
					$scope.Id = tbl_Student.id;
					$scope.studentName = tbl_Student.name;
					$scope.studentEmail = tbl_Student.email;
				}, function() {
			alert('Error in getting records');
		});
	}
});