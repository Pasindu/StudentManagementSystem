app.service("StudentService", function($http) {  
	
	var student = {
			id : 0,
			name : "",
			email : ""
	}
    //get All Students
    this.getAllStudents = function() {  
        return $http.get(SERVICE_URL+'students');
    };  
    // Add Student  
    this.addNewStudent = function(student) {  
            return $http({  
                method: 'POST',  
                url: SERVICE_URL+'saveStudent',  
                data: {'student' : student} 
            });

     };
    // Updating Student  
    this.updateStudent = function(student) {  
            return $http({  
            	method: 'POST',  
                url: SERVICE_URL+'saveStudent',  
                data: {'student' : student}
            });
    };
    //Delete student  
    this.deleteStudent = function(Id) {  
    	return $http({  
        	method: 'DELETE',  
            url: SERVICE_URL+'deleteStudent',  
            params: {'studentId' : Id}
        }); 
    };
    //get particular student
    this.getStudent = function(Id) {  
    	return $http({  
        	method: 'GET',  
            url: SERVICE_URL+'getDetails',  
            params: {'studentId' : Id}
        }); 
    };
    
    this.getStudentMain = function(){
    	return student;
    };
    
});  