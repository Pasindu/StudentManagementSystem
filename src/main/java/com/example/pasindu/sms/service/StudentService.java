package com.example.pasindu.sms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.pasindu.sms.model.Student;
import com.example.pasindu.sms.repository.StudentRepository;

@Service
public class StudentService {

	@Autowired
	private StudentRepository studentRepository;

	public Student getStudentById(Integer id) {

		return studentRepository.findOne(id);

	}
	
	public List<Student> getAllStudents(){
		return studentRepository.findAll();
	}
	
	public boolean saveStudent(Student student){
		boolean status = false;
		try{
			studentRepository.save(student);
			status = true;
		}catch(Exception e){
			System.out.println("Error in JPA save or update");
		}
		
		return status;
	}
	
	public boolean deleteStudent(Integer id){
		boolean status = false;
		
		try{
			studentRepository.delete(id);
			status = true;
		}catch(Exception e){
			System.out.println("Error in JPA delete");
		}
		
		return status;
	}
}
