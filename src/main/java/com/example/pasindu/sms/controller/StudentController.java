package com.example.pasindu.sms.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.pasindu.sms.model.Student;
import com.example.pasindu.sms.service.StudentService;

@RestController
public class StudentController {

	@Autowired
	private StudentService studentService;
	
	@RequestMapping(value = "/saveStudent", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> saveStudent(@RequestBody Student student){
		boolean status = false;
		try{
			status = studentService.saveStudent(student);
		}catch(Exception e){
			
		}
		
		if(status == true){
			return new ResponseEntity<Boolean>(new Boolean(status),HttpStatus.OK);
		}
		
		return new ResponseEntity<Boolean>(new Boolean(status),HttpStatus.NOT_ACCEPTABLE);
	}
	
	@RequestMapping(value = "/deleteStudent", method = RequestMethod.DELETE)
	public ResponseEntity<Boolean> deleteStudent(@RequestParam Integer studentId){
		boolean status = false;
		try{
			status = studentService.deleteStudent(studentId);
		}catch(Exception e){
			
		}
		
		if(status == true){
			return new ResponseEntity<Boolean>(new Boolean(status),HttpStatus.OK);
		}
		
		return new ResponseEntity<Boolean>(new Boolean(status),HttpStatus.NOT_ACCEPTABLE);
	}
	
	@RequestMapping(path = "/students", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Student>> listStudents(){
	
		List<Student> students = Collections.emptyList();

		try{
			students = studentService.getAllStudents();
			
		}catch(Exception e){
			System.out.println("JPA Error...");
		}
		

		if (students.isEmpty()) {
			return new ResponseEntity<List<Student>>(students, HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<List<Student>>(students, HttpStatus.OK);

	}

	@RequestMapping(path = "/getDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Student> getStudent(@RequestParam Integer studentId){
	
		Student student = null;

		try{
			student = studentService.getStudentById(studentId);
			
		}catch(Exception e){
			System.out.println("JPA Error...");
		}
		

		if (student == null) {
			return new ResponseEntity<Student>(student, HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<Student>(student, HttpStatus.OK);

	}
	
}
