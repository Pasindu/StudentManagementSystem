package com.example.pasindu.sms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.example.pasindu.sms.model.Student;

public interface StudentRepository extends JpaRepository<Student, Integer>, CrudRepository<Student, Integer>{

}
